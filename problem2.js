const fsp = require("fs/promises");

function problem2Soln(givenTxtFile)
{
    fsp.readFile(givenTxtFile,'utf8')
    .then((data)=>{
        console.log("1. Done reading the given text file");
        return fsp.writeFile('uppercaseFile.txt', data.toUpperCase());
    })
    .then(()=>{
        console.log("2. Done writing to the uppercaseFile.txt in uppercase");
        return fsp.appendFile("filenames.txt", 'uppercaseFile.txt \n',);
    })
    .then(()=>{
        return fsp.readFile('uppercaseFile.txt', 'utf-8');
    })
    .then((data)=>{
        console.log("3. Done reading the uppercaseFile.txt");
        return fsp.writeFile('lowercaseFile.txt', JSON.stringify(data.toLowerCase().replace(/(\r\n|\n|\r)/gm, "").split('. ').filter((sen) => sen != "")));
    })
    .then(()=>{
        console.log("Done writing to the lowercaseFile.txt in lowercase");
        return fsp.appendFile('filenames.txt', 'lowercaseFile.txt \n');
    })
    .then(()=>{
        return fsp.readFile('lowercaseFile.txt', 'utf-8')
    })
    .then((data)=>{
        console.log("4. Done reading the content in lowercaseFile.txt");
        return fsp.writeFile('sortedFile.txt', JSON.stringify(JSON.parse(data).sort()));
    })
    .then(()=>{
        console.log("Done sorting the content");
        return fsp.appendFile('filenames.txt', 'sortedFile.txt \n');
    })
    .then(()=>{
        return fsp.unlink('uppercaseFile.txt');
    })
    .then(()=>{
        console.log("5. uppercaseFile.txt is deleted");
        return fsp.unlink('lowercaseFile.txt');
    })
    .then(()=>{
        console.log("lowercaseFile.txt is deleted");
        return fsp.unlink('sortedFile.txt');
    })
    .then(()=>{
        console.log("sortedFile.txt is deleted");
    })
    .catch((err)=>console.error(err));
}

module.exports = problem2Soln;