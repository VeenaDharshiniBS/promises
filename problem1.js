const fsp = require("fs/promises");

function createAndDeleteJsonFiles(dirName) {

    let randomNum = Math.floor(Math.random() * 10 + 1);
    console.log(`Random Number: ${randomNum}`);

    const files = Array.from({ length: randomNum }, (_, index) => index + 1);

    fsp.mkdir(dirName)
        .then(() => {
            console.log("Directory created");
            let filePromises = files.map(file => fsp.writeFile(`${dirName}/${file}`, ""));
            return Promise.all(filePromises);
        })
        .then(() => {
            console.log("Files Created");
            let filePromises = files.map(file => fsp.unlink(`${dirName}/${file}`));
            return Promise.all(filePromises);
        })
        .then(() => {
            console.log("Deleted all files");
            return fsp.rmdir(dirName);
        })
        .then(() => {
            console.log("Directory deleted");
        })
        .catch((err) => {
            console.error(err);
        })
}

module.exports = createAndDeleteJsonFiles;
